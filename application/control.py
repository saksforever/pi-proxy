import os
import time
import subprocess
import config

def check_wg():
    try:
        tor = subprocess.check_output(['ifconfig', 'wg'])
        return 'Active'
    except subprocess.CalledProcessError:
        return 'Down'

def check_tor():
    try:
        tor = subprocess.check_output(['systemctl', 'is-active', 'tor'])
        return 'Active'
    except subprocess.CalledProcessError:
        return 'Down'

def check_3proxy():
    try:
        proxy3 = subprocess.check_output(['systemctl', 'is-active', '3proxy'])
        return 'Active'
    except subprocess.CalledProcessError:
        return 'Down'

def check_dnsproxy():
    try:
        dnsproxy = subprocess.check_output(['systemctl', 'is-active', 'dnsproxy'])
        return 'Active'
    except subprocess.CalledProcessError:
        return 'Down'

def check_mode():
    f = open(config.proxy3_conf,'r').readline().rstrip()
    if f == '#1':
        return 'TOR Mode'
    elif f == '#2':
        return 'TOR-SOCKS'
    elif f == '#3':
        return 'TOR-SOCKS-AUTH'
    elif f == '#4':
        return 'NO TOR/SOCKS'
    else:
        return 'Unknown Mode'

def system_status():
    uptime = subprocess.check_output(["uptime"])
    return str(uptime,'utf-8')

def save_proxy_cfg(conf):
    f = open(config.proxy3_conf, 'w')
    for i in range(len(conf)):
        f.write(conf[i] + "\n")
    f.close()

def start_wg():
    os.system('wg-quick up /etc/wireguard/wg.conf' )

def stop_wg():
    os.system('wg-quick down /etc/wireguard/wg.conf')

def restart_3proxy():
    os.system('systemctl restart 3proxy')

def restart_dnsproxy():
    os.system('systemctl restart dnsproxy')
