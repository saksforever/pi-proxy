import control
import config
from flask import Flask, render_template, request, redirect, url_for, flash
app = Flask(__name__)

@app.route('/')
def index():
    uptime = control.system_status()
    mode = control.check_mode()
    tor_status = control.check_tor()
    wg_status = control.check_wg()
    proxy_status = control.check_3proxy()
    dnsproxy_status = control.check_dnsproxy()
    return render_template('index.html',uptime=uptime,mode=mode,wg_status=wg_status,tor_status=tor_status,proxy_status=proxy_status,dnsproxy_status=dnsproxy_status)
    
@app.route('/wireguard/edit', methods=['GET', 'POST'])
def wg_edit():
    wg_status = control.check_wg()
    f = open(config.wg_conf, 'r').read()
    if request.method == "POST" :
        wgedit = request.form['wgedit']
        f = open(config.wg_conf, 'w').write(wgedit)
        if request.form.get('start') == 'start wg':
            control.start_wg()
        elif  request.form.get('stop') == 'stop wg':
            control.stop_wg()  
        return redirect(url_for('wg_edit'))
    return render_template('wg.html', wg_conf=f, wg_status=wg_status)

@app.route('/dns/edit', methods=['GET', 'POST'])
def dns_edit():
    f = open(config.dns_conf, 'r').readline()
    if request.method == "POST":
        dns_edit = request.form['dnsedit']
        f = open(config.dns_conf, 'w').writelines(dns_edit + '\n')
        control.restart_dnsproxy()
        return redirect(url_for('dns_edit'))
    return render_template('dns_proxy.html', dns_edit=f)

@app.route('/socks/edit', methods=['GET', 'POST'])
def socks_edit():
    conf = ['internal 192.168.22.1','daemon', 'plugin /lib/3proxy/TransparentPlugin.ld.so transparent_plugin', 'flush','auth iponly','allow * * * *','parent 1000 socks5 127.0.0.1 9050','tcppm -i192.168.22.1 6666 127.0.0.1 11111']
    if request.method == 'POST' and request.form['workmode'] == '4':
        conf[6]='#'
        conf.insert(0,'#4')
        control.save_proxy_cfg(conf)
        control.restart_3proxy()
    elif request.method == 'POST' and request.form['workmode'] == '3':
        socks_ip = request.form['tsocksa_ip']
        socks_port = request.form['tsocksa_port']
        socks_login = request.form['tsocksa_lg']
        socks_pass = request.form['tsocksa_pw']
        conf.insert(0,'#3')
        conf.insert(8,'parent 1000 socks5 ' + socks_ip + ' ' + socks_port + ' ' + socks_login + ' ' + socks_pass)
        control.save_proxy_cfg(conf)
        control.restart_3proxy()
    elif request.method == 'POST' and request.form['workmode'] == '2':
        socks_ip = request.form['tsocks_ip']
        socks_port = request.form['tsocks_port']
        conf.insert(0,'#2')
        conf.insert(8,'parent 1000 socks5 ' + socks_ip + ' ' + socks_port)
        control.save_proxy_cfg(conf)
        control.restart_3proxy()
    elif request.method == 'POST' and request.form['workmode'] == '1':
        conf.insert(0, '#1')
        control.save_proxy_cfg(conf)
        control.restart_3proxy()

    return render_template('socks.html')

@app.route('/system/reboot')
def reboot():
    control.reboot_router()

if __name__ == '__main__':
    app.run(host='0.0.0.0')
    