#!/bin/bash

apt-get update
apt-get upgrade -y
apt-get install -y mc htop psmisc git make gcc python3-pip hostapd iptables-persistent tor tor-arm dnsmasq netdiag tcpdump

#Disable some garbage
systemctl disable avahi-daemon
systemctl stop avahi-daemon

#Stop curent service | we will start it later
systemctl stop tor
systemctl stop dnsmasq

# Install 3proxy
git clone https://github.com/z3APA3A/3proxy
cd 3proxy/ && make -f Makefile.Linux
mkdir -p /lib/3proxy/
mv bin/TransparentPlugin.ld.so /lib/3proxy/
mv bin/PCREPlugin.ld.so /lib/3proxy/
mv bin/TrafficPlugin.ld.so /lib/3proxy/
mv bin/StringsPlugin.ld.so /lib/3proxy/
mv bin/3proxy /usr/bin/
cd ..
rm -rf 3proxy

# Install dnsproxy
git clone https://github.com/jtripper/dns-tcp-socks-proxy
cd dns-tcp-socks-proxy && make
mkdir -p /etc/dnsproxy
mv dns_proxy.conf /etc/dnsproxy/
mv dns_proxy /usr/bin/
cd ..
rm -rf dns-tcp-socks-proxy

#Install configs 
cp conf/dnsmasq.conf /etc/dnsmasq.conf
cp conf/dns_proxy.conf /etc/dnsproxy/dns_proxy.conf
cp conf/resolv.conf /etc/dnsproxy/resolv.conf
cp conf/hostapd.conf /etc/hostapd/hostapd.conf
cp conf/interfaces /etc/network/interfaces
cp conf/sysctl.conf /etc/sysctl.conf
cp conf/wvdial.conf /etc/wvdial.conf
cp conf/3proxy.cfg /etc/3proxy.cfg
cp conf/service/3proxy.service /lib/systemd/system/
cp conf/service/dnsproxy.service /lib/systemd/system/
cp conf/service/webmanager.service /lib/systemd/system/
sed -i /etc/default/hostapd -e '/DAEMON_CONF=/c DAEMON_CONF="/etc/hostapd/hostapd.conf"'

#Install web app
mv appplication /opt/

## Install python dep
pip3 install flask
pip3 install requests
pip3 install pysocks

## Install & run our & other service
systemctl enable 3proxy
systemctl enable dnsproxy
systemctl enable webmanager
systemctl enable dnsmasq
systemctl enable tor
systemctl enable hostapd

#Install & save iptables rules
iptables -t nat -A PREROUTING -i wlan0 -p tcp --dport 22 -j REDIRECT --to-ports 22
iptables -t nat -A PREROUTING -i wlan0 -p tcp --dport 5000 -j REDIRECT --to-ports 5000
iptables -t nat -A PREROUTING -i wlan0 -p tcp --syn -j REDIRECT --to-ports 6666
iptables-save > /etc/iptables/rules.v4

echo "don't dorget to enable wifi location in raspi-config"
reboot